package net.nilosplace;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class LineReader extends Thread {

	private BufferedReader read;
	private InputStream in;
	
	public LineReader(InputStream in) {
		this.in = in;
		//read = new BufferedReader(new InputStreamReader(in));
	}

	public void run() {
		try {
			int ch;
			do {
				ch=in.read();
				if (ch<0) return;
				System.out.print((char)ch);
				System.out.flush();
			} while(true);
			
			//while((line = read.readLine()) != null) {
			//	System.out.println(line);
			//	System.out.flush();
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
