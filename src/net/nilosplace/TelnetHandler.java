package net.nilosplace;
import thor.net.DefaultTelnetTerminalHandler;
import thor.net.TelnetConstants;

public class TelnetHandler extends DefaultTelnetTerminalHandler implements TelnetConstants {
	public void LineFeed() {
		System.out.print('\n');
		System.out.flush();
	}
	//public void CarriageReturn() {
	//	System.out.print('\r');
	//	System.out.flush();
	//}
	public void BackSpace() {
		System.out.print((char)BS);
		System.out.flush();
	}
	public void HorizontalTab() {
		System.out.print((char)HT);
		System.out.flush();
	}
}