package net.nilosplace;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import thor.net.TelnetURLConnection;
import thor.net.URLStreamHandler;


public class Chessy {

	volatile boolean closed = false;
	public static void main(String[] args) throws IOException {
		new Chessy();
	}
	
	public Chessy() throws IOException {
		
		TelnetHandler th = new TelnetHandler();
		URL url = new URL("telnet", "freechess.org", 5000, "", new URLStreamHandler());
		URLConnection urlConnection = url.openConnection();
		urlConnection.connect();
		if (urlConnection instanceof TelnetURLConnection) {
			((TelnetURLConnection)urlConnection).setTelnetTerminalHandler(th);
		}
	
		LineReader reader = new LineReader(urlConnection.getInputStream());
		reader.start();
		LineWriter writer = new LineWriter(urlConnection.getOutputStream());
		writer.start();

		try {
			while(writer.isAlive()) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		((TelnetURLConnection)urlConnection).disconnect();

	}

}
