javadoc -sourcepath . \
              -d ./docs                          \
              -use                               \
              -splitIndex                        \
              -windowtitle 'Thor Library'        \
              -doctitle 'Thor Library'           \
              -group "Thor Packages" "thor.*"    \
              -J-Xmx180m \
	      thor.app thor.net
              
